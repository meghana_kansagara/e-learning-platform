function updateCourse(name,author,courseId,description){
	var row = '<div class="mx-auto">';
  	row += '<div class="card shadow-lg p-3 mb-5 bg-white rounded " data-aos="zoom-in" data-aos-delay="400" style="width: 18rem;">';
  	row += '<div class="card-body">';
    row += '<h5 class="card-title">'+name+'</h5>';
    row += '<h6 class="card-subtitle mb-2 text-muted">'+author+'</h6><br>';
    row += '<p class="card-text">'+description+'</p>';
    row += '<a href="Courses?id='+courseId+'" class="card-link">Tutorial</a></div></div></div>';
	$('#listcourses').append(row);
}
var urlParams = new URLSearchParams(window.location.search);
var CourseId = urlParams.get('id');//CourseId=2
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
       if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       var jsonCourses = JSON.parse(this.response);
       for(course of jsonCourses){
       	updateCourse(course.courseName,course.courseAuthor,course.id,course.courseDescription);
       }
       
    }
};
xhttp.open("GET", "/courses?id="+CourseId, true);
xhttp.send();
