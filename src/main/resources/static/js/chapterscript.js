function insertNewRecord(data) {
  var table = document.getElementById("chapterlist").getElementsByTagName('tbody')[0];
  var newRow = table.insertRow(table.length);
  cell1 = newRow.insertCell(0);
  cell1.innerHTML = data.id;
  cell1 = newRow.insertCell(1);
  cell1.innerHTML = data.CourseId;
  cell2 = newRow.insertCell(2);
  cell2.innerHTML = data.chapterName;
  cell3 = newRow.insertCell(3);
  cell3.innerHTML = data.shortNote;
  cell4 = newRow.insertCell(4);
  cell4.innerHTML = data.videoLocation;
  cell4 = newRow.insertCell(5);
  cell4.innerHTML = data.chapterDescription;
  cell4 = newRow.insertCell(6);
  cell4.innerHTML = `<a href="#editChapterModal" class="edit" data-toggle="modal" onClick="onEdit(this)"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                      <a  onclick="onDelete(\``+data.id+`\`)"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>`;
}

function onUpdate(id){
  var file = document.getElementById("editvideolocation").files[0];
  var fileData = new FormData();
  fileData.append("file",file);
  var formData = {};
  formData.id = id;
  formData.courseId = CourseId;
  formData.chapterDescription = document.getElementById("editchapterdescription").value;
  formData.chapterName = document.getElementById("editchaptername").value;
  formData.shortNote = document.getElementById("editshortnote").value;

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if(this.readyState==4 && this.status==200){

      var response = JSON.parse(this.responseText);
      if(response.key.includes("success")){
        formData.videoLocation = response.value;
        updateData(formData);
      }
    }
  };
  xhttp.open("POST","/upload", true);
  xhttp.send(fileData);
}

function onEdit(td){
  selectedRow = td.parentElement.parentElement;
  document.getElementById("editchaptername").value = selectedRow.cells[2].innerHTML;
  document.getElementById("editshortnote").value = selectedRow.cells[3].innerHTML;
  document.getElementById("editchapterdescription").value = selectedRow.cells[5].innerHTML;
  var updateButton = '<input type="button" onClick="onUpdate('+selectedRow.cells[0].innerHTML+')" class="btn btn-info" value="Save">';
  $('editUpdateButton').empty();
  $('editUpdateButton').append(updateButton);
}


function onFormSubmit() {
  var formData = readFormData();

  var fileData = new FormData();
  fileData.append("file",formData.video);


  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if(this.readyState==4 && this.status==200){

      var response = JSON.parse(this.responseText);
      if(response.key.includes("success")){
        formData.videoLocation = response.value;
        uploadData(formData);
      }
    }
  };
  xhttp.open("POST","/upload", true);
  xhttp.send(fileData);


}

function onDelete(id){
  if(confirm('Are you sure to delete this record ?')){
    var deleteRequest = new XMLHttpRequest();
    deleteRequest.onreadystatechange = function(){
      if(this.readyState==4 && this.status==200){
        location.reload();
      }
    }
    deleteRequest.open("DELETE","/chapterManipulation/chapterDelete/"+id,true);
    deleteRequest.send();

  }

}

function updateData(formData){

  delete formData['video'];

  var addChapter  = new XMLHttpRequest();
  addChapter.onreadystatechange = function(){
    if(this.readyState==4 && this.status==200){
      var response = JSON.parse(this.responseText);
      if(response.key.includes("Success")){
        location.reload();
      }
    }

  }
  addChapter.open("PUT","/chapterManipulation/chapterUpdate",true);
  addChapter.setRequestHeader("Content-Type","application/json");
  addChapter.send(JSON.stringify(formData));
}


function uploadData(formData){

  delete formData['video'];

  var addChapter  = new XMLHttpRequest();
  addChapter.onreadystatechange = function(){
    if(this.readyState==4 && this.status==200){
      var response = JSON.parse(this.responseText);
      if(response.key.includes("success")){
        location.reload();
      }
    }

  }
  addChapter.open("POST","/chapterManipulation",true);
  addChapter.setRequestHeader("Content-Type","application/json");
  addChapter.send(JSON.stringify(formData));
}
function readFormData() {
  var formData = {};
  formData["courseId"] = CourseId;
  formData["chapterName"] = document.getElementById("chaptername").value;
  formData["shortNote"] = document.getElementById("shortnote").value;
  formData["video"] = document.getElementById("videolocation").files[0];
  formData["chapterDescription"] = document.getElementById("chapterdescription").value;
  return formData;
}


var urlParams = new URLSearchParams(window.location.search);
var CourseId = urlParams.get('id');//CourseId=2


var xmlhttpread = new XMLHttpRequest();
xmlhttpread.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    var myArr = JSON.parse(this.responseText);

    for(var chapters of myArr){
      chapters.CourseId = CourseId;

      insertNewRecord(chapters);
    }
  }
};
xmlhttpread.open("GET","/chapters?id="+CourseId, true);
xmlhttpread.send();