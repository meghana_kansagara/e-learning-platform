function onFormSubmit() {
	 var formData = readFormData();
	 var xhttp = new XMLHttpRequest();
     xhttp.onreadystatechange = function() {
     location.reload();
  };
  xhttp.open("POST","/courseManipulation", true);
  xhttp.setRequestHeader("Content-type", "application/json");
  xhttp.send(JSON.stringify(formData));
}
function readFormData() {
	var formData = {};

    formData["categoryId"] = document.getElementById("categoryid").value;
    formData["courseName"] = document.getElementById("coursename").value;
    formData["courseAuthor"] = document.getElementById("authorname").value;
    formData["courseDescription"] = document.getElementById("coursedescription").value;
    return formData;
}

function insertNewRecord(data) {
    var table = document.getElementById("courselist").getElementsByTagName('tbody')[0];
    var newRow = table.insertRow(table.length);
    cell1 = newRow.insertCell(0);
    cell1.innerHTML = data.id;
    cell1 = newRow.insertCell(1);
    cell1.innerHTML = data.categoryId;
    cell2 = newRow.insertCell(2);
    cell2.innerHTML = data.courseName;
    cell3 = newRow.insertCell(3);
    cell3.innerHTML = data.courseAuthor;
    cell4 = newRow.insertCell(4);
    cell4.innerHTML = data.courseDescription;
    cell4 = newRow.insertCell(5);
    cell4.innerHTML = `<a href="#editCourseModal" class="edit" data-toggle="modal" onClick="onEdit(this,\``+data.id+`\`)"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>
                      <a  onclick="onDelete(\``+data.id+`\`)"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>
					  <a href="/chapteradmin?id=`+data.id+`" class="view" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="view">&#xE417;</i></a>`;
}


function onEdit(td) {
    selectedRow = td.parentElement.parentElement;
    document.getElementById("editcategoryid").value = selectedRow.cells[1].innerHTML;
    document.getElementById("editcoursename").value = selectedRow.cells[2].innerHTML;
    document.getElementById("editauthorname").value = selectedRow.cells[3].innerHTML;
    document.getElementById("editcoursedescription").value = selectedRow.cells[4].innerHTML;
    var updateButton = '<input type="button" onClick="onUpdate('+selectedRow.cells[0].innerHTML+')" class="btn btn-info" value="Save">';
    $('editUpdateButton').empty();
    $('editUpdateButton').append(updateButton);
}
function updateRecord(formData) {
    selectedRow.cells[0].innerHTML = formData.categoryid;
    selectedRow.cells[1].innerHTML = formData.coursename;
    selectedRow.cells[2].innerHTML = formData.authorname;
    selectedRow.cells[3].innerHTML = formData.coursedescription;
}

function onUpdate(id){
    var form = {};
    form.id = id;
    console.log(id);
    form.courseName = document.getElementById("editcoursename").value;
    form.courseDescription = document.getElementById("editcoursedescription").value;
    form.courseAuthor = document.getElementById("editauthorname").value;
    form.categoryId = document.getElementById("editcategoryid").value;
    var updateRequest = new XMLHttpRequest();
    updateRequest.onreadystatechange = function (){
        if(this.readyState==4 && this.status==200){
            location.reload();
        }
    }

    updateRequest.open("PUT","/courseManipulation/courseUpdate",true);
    updateRequest.setRequestHeader("Content-Type","application/json");
    updateRequest.send(JSON.stringify(form));
    console.log(form);

}

function onDelete(id) {
    if (confirm('Are you sure to delete this record ?')) {
        console.log(id);
        var deleteRequest = new XMLHttpRequest();
        deleteRequest.onreadystatechange = function (){
            if(this.readyState==4 && this.status==200){
                location.reload();
            }
        }
        deleteRequest.open("DELETE","/courseManipulation/courseDelete/"+id,true);
        deleteRequest.send();

    }
}
var xmlhttpread = new XMLHttpRequest();
xmlhttpread.onreadystatechange = function() {
  if (this.readyState == 4 && this.status == 200) {
    var myArr = JSON.parse(this.responseText);
    for(var course of myArr){
        insertNewRecord(course);
    }
  }
};
xmlhttpread.open("GET","/listallcourses", true);
xmlhttpread.send();

 