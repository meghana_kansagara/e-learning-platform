var chapterNumber=1;
function addChapter(name,note,id,video,description){
	var row = '<div class="card mx-auto mx-md-5">';
  	row += '<h5 class="card-header">Chapter '+chapterNumber+'</h5>';
  	row += '<div class="card-body">';
    row += '<h5 class="card-title">'+name+'</h5>';
    row += '<p class="card-text">'+note+'</p>';
    row += '<video width="320" height="240" controls>';
    row += '<source src="'+video+'" type="video/mp4">Your browser does not support the video tag.</video>';
    row += '<p class="card-text">'+description+'</p>';
    $('#chapter').append(row);
  	chapterNumber++;
}

var urlParams = new URLSearchParams(window.location.search);
var CourseId = urlParams.get('id');
var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
       // Typical action to be performed when the document is ready:
       
       var jsonChapters = JSON.parse(this.response);
       for(chapter of jsonChapters){
       		addChapter(chapter.chapterName,chapter.shortNote,chapter.id,chapter.videoLocation,chapter.chapterDescription); 
       }
       
    }
};
xhttp.open("GET", "/chapters?id="+CourseId, true);
xhttp.send();

