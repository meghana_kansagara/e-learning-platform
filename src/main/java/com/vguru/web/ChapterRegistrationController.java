package com.vguru.web;

import java.util.Optional;

import com.vguru.config.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vguru.model.Chapter;
import com.vguru.model.Courses;
import com.vguru.repository.ChapterRepository;
import com.vguru.repository.CoursesRepository;
import com.vguru.web.dto.ChapterDto;

@Controller
@RequestMapping("/chapterManipulation")
public class ChapterRegistrationController {

	@Autowired
	ChapterRepository chapterRepo;
	
	@Autowired
	CoursesRepository coursesRepo;
	
	@PostMapping
	@ResponseBody
	public ResponseMessage registerChapter(@RequestBody ChapterDto chapterDto )
	{
		Courses course = coursesRepo.findById(chapterDto.getCourseId());
		Chapter chapter = new Chapter();
		chapter.setShortNote(chapterDto.getShortNote());
		chapter.setChapterName(chapterDto.getChapterName());
		chapter.setChapterDescription(chapterDto.getChapterDescription());
		chapter.setVideoLocation(chapterDto.getVideoLocation());
		chapter.setCourse(course);
		chapterRepo.save(chapter);
		ResponseMessage message = new ResponseMessage();
		message.key="success";
		message.value="reload";
		return message;
	}
	
	@DeleteMapping("/chapterDelete/{id}")
	@ResponseBody
	public String deleteChapter(@PathVariable int id) {
		
		Chapter chapter = chapterRepo.findById(id);
		chapterRepo.delete(chapter);
		return "success";
	}
	
	@PutMapping("/chapterUpdate")
	@ResponseBody
	public ResponseMessage updateChapter(@RequestBody ChapterDto chapterDto)
	{

		Courses course = coursesRepo.findById(chapterDto.getCourseId());
		Chapter chapter = new Chapter();
		chapter.setId(chapterDto.getId());
		chapter.setShortNote(chapterDto.getShortNote());
		chapter.setChapterName(chapterDto.getChapterName());
		chapter.setChapterDescription(chapterDto.getChapterDescription());
		chapter.setVideoLocation(chapterDto.getVideoLocation());
		chapter.setCourse(course);
		chapterRepo.save(chapter);
		ResponseMessage message = new ResponseMessage();
		message.key = "Success";
		message.value="reload page";
		return message;
	}
}
