package com.vguru.web;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;

import com.vguru.config.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;


@Controller
public class FileController {
	//Save the uploaded file to this folder
    private static String UPLOADED_FOLDER = "C:\\projects\\vguru-web-applicaion\\src\\main\\resources\\static\\videos\\";

	
    
    @PostMapping("/upload") // //new annotation since 4.3
    @ResponseBody
    public ResponseMessage singleFileUpload(@RequestParam("file") MultipartFile file,
                                                  RedirectAttributes redirectAttributes) {
    	//String UPLOADED_FOLDER = ServletUriComponentsBuilder.fromCurrentContextPath().path("/videos/").toUriString();
        if (file.isEmpty()) {
            redirectAttributes.addFlashAttribute("message", "Please select a file to upload");
            ResponseMessage message = new ResponseMessage();
            message.key = "failed";
            message.value = "upload again";

            return message;
        }

        try {

            // Get the file and save it somewhere
            byte[] bytes = file.getBytes();
            Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
            Files.write(path, bytes);

            redirectAttributes.addFlashAttribute("message",
                    "You successfully uploaded '" + file.getOriginalFilename() + "'");

        } catch (IOException e) {
            e.printStackTrace();
        }

        ResponseMessage message = new ResponseMessage();
        message.key = "success";
        message.value="videos/"+file.getOriginalFilename();


        return message;
    }

 
}
