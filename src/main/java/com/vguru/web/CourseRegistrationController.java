package com.vguru.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vguru.model.Chapter;
import com.vguru.model.Courses;
import com.vguru.repository.ChapterRepository;
import com.vguru.repository.CoursesRepository;
import com.vguru.web.dto.CoursesDto;

@Controller
@RequestMapping("/courseManipulation")
public class CourseRegistrationController {
	
	@Autowired
	CoursesRepository courseRepo;
	
	@Autowired
	ChapterRepository chapterRepo;
	
	@PostMapping
	@ResponseBody
	public String registerCourse(@RequestBody CoursesDto courseDto) {
		Courses course = new Courses();
		course.setCourseName(courseDto.getCourseName());
		course.setCategoryId(courseDto.getCategoryId());
		course.setCourseDescription(courseDto.getCourseDescription());
		course.setCourseAuthor(courseDto.getCourseAuthor());
		courseRepo.save(course);
		return "success";
		
	}
	
	@DeleteMapping("/courseDelete/{id}")
	@ResponseBody
	public String deleteCourse(@PathVariable int id) {
		Courses course = courseRepo.findById(id);
		List<Chapter> listOfChapters = chapterRepo.findByCourse(course);
		chapterRepo.deleteAll(listOfChapters);
		courseRepo.delete(course);
		return "Success";
	}
	
	@PutMapping("/courseUpdate")
	@ResponseBody
	public String updateCourse(@RequestBody CoursesDto courseDto)
	{
		Courses course = new Courses();
		course.setId(courseDto.getId());
		course.setCourseName(courseDto.getCourseName());
		course.setCategoryId(courseDto.getCategoryId());
		course.setCourseDescription(courseDto.getCourseDescription());
		course.setCourseAuthor(courseDto.getCourseAuthor());
		courseRepo.save(course);
		return "success";
	}
}
