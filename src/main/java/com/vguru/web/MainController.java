package com.vguru.web;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.vguru.model.Chapter;
import com.vguru.model.Courses;
import com.vguru.model.User;
import com.vguru.repository.ChapterRepository;
import com.vguru.repository.CoursesRepository;
import com.vguru.repository.UserRepository;
import com.vguru.service.UserService;




@Controller
public class MainController {
	
	@Autowired
	UserRepository userRepo;
	
	@Autowired
	CoursesRepository coursesRepo;
	
	@Autowired
	ChapterRepository chapterRepo;
	
    @GetMapping("/")
    public String root() {
        return "vgurumain";
    }
    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @GetMapping("/Courses")
    public String course(@RequestParam int id,Model model) {
    	model.addAttribute("id",id);
    	return "Courses";
    }
    @GetMapping("/primary")
    public String primary(@RequestParam int id,Model model) {
    	model.addAttribute("id",id);
    	return "primary";
    }
    
	@GetMapping("/adminmain")
	public String adminmain(Model model)
	{
		return "adminmain";
	}
	
	@GetMapping("/courses")
	@ResponseBody
	public List<Courses> listAllCourses(@RequestParam int id,Model model)
	{
		List<Courses> courses = coursesRepo.findByCategoryId(id);
		return courses;
	}
	
	@GetMapping("/chapters")
	@ResponseBody
	public List<Chapter> listChapters(@RequestParam int id)
	{
		Courses course = coursesRepo.findById(id);
		List<Chapter> chapters = chapterRepo.findByCourse(course);
		
		return chapters;
	}
	@GetMapping("/listallcourses")
	@ResponseBody
	public List<Courses> listAllCoursesbyid(Model model)
	{
		List<Courses> courses = coursesRepo.findAll();
		return courses;
	}
	@GetMapping("/chapteradmin")
	public String listadminchapter(@RequestParam int id,Model model) {
    	model.addAttribute("id",id);
    	return "chapter";
    }
	
	
}
