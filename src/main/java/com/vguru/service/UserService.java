package com.vguru.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.vguru.model.User;
import com.vguru.web.dto.UserRegistrationDto;

public interface UserService extends UserDetailsService {

    User findByEmail(String email);

    User save(UserRegistrationDto registration);
}
