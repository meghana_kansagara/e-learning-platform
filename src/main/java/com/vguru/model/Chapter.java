package com.vguru.model;

import java.util.Optional;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Chapter {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String chapterName;
	private String chapterDescription;
	private String shortNote;
	private String videoLocation;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.DETACH)
	private Courses course;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getChapterName() {
		return chapterName;
	}

	public void setChapterName(String chapterName) {
		this.chapterName = chapterName;
	}

	public String getChapterDescription() {
		return chapterDescription;
	}

	public void setChapterDescription(String chapterDescription) {
		this.chapterDescription = chapterDescription;
	}

	public String getShortNote() {
		return shortNote;
	}

	public void setShortNote(String shortNote) {
		this.shortNote = shortNote;
	}

	public String getVideoLocation() {
		return videoLocation;
	}

	public void setVideoLocation(String videoLocation) {
		this.videoLocation = videoLocation;
	}

	public Courses getCourse() {
		return course;
	}

	public void setCourse(Courses course2) {
		this.course = course2;
	}

	@Override
	public String toString() {
		return "Chapter [id=" + id + ", chapterName=" + chapterName + ", chapterDescription=" + chapterDescription
				+ ", shortNote=" + shortNote + ", videoLocation=" + videoLocation + ", course=" + course + "]";
	}

}
