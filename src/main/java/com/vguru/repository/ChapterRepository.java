package com.vguru.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vguru.model.Chapter;
import com.vguru.model.Courses;

@Repository
public interface ChapterRepository extends JpaRepository<Chapter, Integer> {
	List<Chapter> findByCourse(Courses course);
	Chapter findById(int id);
}
