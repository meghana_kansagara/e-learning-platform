package com.vguru.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.vguru.model.Courses;

@Repository
public interface CoursesRepository extends JpaRepository<Courses, Integer>{
	Courses findById(int id);
	List<Courses> findByCategoryId(int id);
}
